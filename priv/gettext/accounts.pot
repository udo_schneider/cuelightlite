## This file is a PO Template file.
##
## "msgid"s here are often extracted from source code.
## Add new translations manually only if they're dynamic
## translations that can't be statically extracted.
##
## Run "mix gettext.extract" to bring this file up to
## date. Leave "msgstr"s empty as changing them here as no
## effect: edit them in PO (.po) files instead.
msgid ""
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/layout/root.html.leex:67
msgid "Account"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/edit.html.eex:25
#: lib/cue_light_lite_web/templates/pow/registration/new.html.eex:26 lib/cue_light_lite_web/templates/pow/session/new.html.eex:17
msgid "E-Mail"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/edit.html.eex:13
#: lib/cue_light_lite_web/templates/pow/registration/new.html.eex:14
msgid "Family Name"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/edit.html.eex:19
#: lib/cue_light_lite_web/templates/pow/registration/new.html.eex:20
msgid "Given Name"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/edit.html.eex:39
msgid "Locale"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/layout/root.html.leex:55
msgid "Login"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/edit.html.eex:47
msgid "New Password"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/new.html.eex:46
#: lib/cue_light_lite_web/templates/pow/session/new.html.eex:23
msgid "Password"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/edit.html.eex:53
#: lib/cue_light_lite_web/templates/pow/registration/new.html.eex:52
msgid "Password Confirmation"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/session/new.html.eex:30
msgid "Remember me"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/new.html.eex:57
msgid "Sign Up"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/new.html.eex:61
#: lib/cue_light_lite_web/templates/pow/session/new.html.eex:5 lib/cue_light_lite_web/templates/pow/session/new.html.eex:33
msgid "Sign in"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/layout/root.html.leex:70
msgid "Sign out"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/layout/root.html.leex:52
#: lib/cue_light_lite_web/templates/pow/registration/new.html.eex:2 lib/cue_light_lite_web/templates/pow/session/new.html.eex:37
msgid "Sign up"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/edit.html.eex:31
msgid "Timezone"
msgstr ""

#, elixir-format
#: lib/cue_light_lite_web/templates/pow/registration/edit.html.eex:2
msgid "Edit profile"
msgstr ""
