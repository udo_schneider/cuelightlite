defmodule CueLightLite.Repo.Migrations.AddNotesToSegment do
  use Ecto.Migration

  def change do
    alter table(:segments) do
      add :notes, :text
    end
  end
end
