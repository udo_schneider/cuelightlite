defmodule CueLightLite.Repo.Migrations.CreateSegments do
  use Ecto.Migration

  def change do
    create table(:segments) do
      add :position, :string
      add :estimated_duration, :integer, default: 0
      add :actual_duration, :integer, default: 0

      timestamps(type: :utc_datetime)
    end
  end
end
