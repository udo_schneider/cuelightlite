defmodule CueLightLite.Repo.Migrations.RenameRundownEndToFinish do
  use Ecto.Migration

  def change do
    rename table(:rundowns), :end, to: :finish
  end
end
