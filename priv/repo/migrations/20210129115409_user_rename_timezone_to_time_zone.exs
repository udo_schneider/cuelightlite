defmodule CueLightLite.Repo.Migrations.UserRenameTimezoneToTimeZone do
  use Ecto.Migration

  def change do
    rename table(:users), :timezone, to: :time_zone
  end
end
