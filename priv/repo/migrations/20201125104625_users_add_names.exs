defmodule CueLightLite.Repo.Migrations.UsersAddNames do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :given_name, :string
      add :family_name, :string
    end
  end
end
