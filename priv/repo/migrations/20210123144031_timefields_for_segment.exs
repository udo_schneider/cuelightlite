defmodule CueLightLite.Repo.Migrations.TimefieldsForSegment do
  use Ecto.Migration

  def change do
    alter table(:segments) do
      remove :actual_duration
      remove :estimated_duration

      add :planned_start, :utc_datetime
      add :planned_duration, :integer
      # add :planned_finish

      add :estimated_start, :utc_datetime
      # add :estimated_finish

      add :actual_start, :utc_datetime
      add :actual_duration, :integer
      # add :actual_finish
    end
  end
end
