defmodule CueLightLite.Repo.Migrations.AddTitleToSegment do
  use Ecto.Migration

  def change do
    alter table(:segments) do
      add :title, :string
    end
  end
end
