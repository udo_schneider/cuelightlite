defmodule CueLightLite.Repo.Migrations.SegmentAddPositionInteger do
  use Ecto.Migration

  def change do
    alter table(:segments) do
      add :position, :integer, null: false
    end
  end
end
