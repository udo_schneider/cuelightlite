defmodule CueLightLite.Repo.Migrations.SegmentRemovePositionString do
  use Ecto.Migration

  def change do
    alter table(:segments) do
      remove :position
    end
  end
end
