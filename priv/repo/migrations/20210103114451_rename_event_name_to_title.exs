defmodule CueLightLite.Repo.Migrations.RenameEventNameToTitle do
  use Ecto.Migration

  def change do
    rename table(:events), :name, to: :title
  end
end
