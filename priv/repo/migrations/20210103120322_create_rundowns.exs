defmodule CueLightLite.Repo.Migrations.CreateRundowns do
  use Ecto.Migration

  def change do
    create table(:rundowns) do
      add :title, :string
      add :start, :utc_datetime
      add :end, :utc_datetime
      add :event_id, references(:events, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:rundowns, [:event_id])
  end
end
