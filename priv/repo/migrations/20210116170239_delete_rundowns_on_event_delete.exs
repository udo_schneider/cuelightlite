defmodule CueLightLite.Repo.Migrations.DeleteRundownsOnEventDelete do
  use Ecto.Migration

  def change do
    alter table(:rundowns) do
      modify :event_id, references(:events, on_delete: :delete_all),
        from: references(:person, on_delete: :nothing)
    end
  end
end
