defmodule CueLightLite.Repo.Migrations.RundownNonNullEvent do
  use Ecto.Migration

  def change do
    alter table(:rundowns) do
      modify :event_id, references(:events, on_delete: :delete_all),
        from: references(:person, on_delete: :delete_all),
        null: false
    end
  end
end
