defmodule CueLightLite.Repo.Migrations.SwitchDbTimestampsToTimestamptz do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :inserted_at, :timestamptz, from: :utc_datetime
      modify :updated_at, :timestamptz, from: :utc_datetime
    end

    alter table(:rundowns) do
      modify :inserted_at, :timestamptz, from: :utc_datetime
      modify :updated_at, :timestamptz, from: :utc_datetime
      modify :start, :timestamptz, from: :utc_datetime
      modify :finish, :timestamptz, from: :utc_datetime
    end

    alter table(:segments) do
      modify :inserted_at, :timestamptz, from: :utc_datetime
      modify :updated_at, :timestamptz, from: :utc_datetime
      modify :planned_start, :timestamptz, from: :utc_datetime
      modify :planned_finish, :timestamptz, from: :utc_datetime
      modify :estimated_start, :timestamptz, from: :utc_datetime
      modify :estimated_finish, :timestamptz, from: :utc_datetime
      modify :actual_start, :timestamptz, from: :utc_datetime
      modify :actual_finish, :timestamptz, from: :utc_datetime
    end
  end
end
