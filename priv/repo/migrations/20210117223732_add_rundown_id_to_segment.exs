defmodule CueLightLite.Repo.Migrations.AddRundownIdToSegment do
  use Ecto.Migration

  def change do
    alter table(:segments) do
      add :rundown_id, references(:rundowns, on_delete: :delete_all), null: false
    end
  end
end
