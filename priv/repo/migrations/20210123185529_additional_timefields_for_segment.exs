defmodule CueLightLite.Repo.Migrations.AdditionalTimefieldsForSegment do
  use Ecto.Migration

  def change do
    alter table(:segments) do
      add :planned_finish, :utc_datetime
      add :estimated_finish, :utc_datetime
      add :actual_finish, :utc_datetime
    end
  end
end
