# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :cue_light_lite,
  ecto_repos: [CueLightLite.Repo]

# Configures the endpoint
config :cue_light_lite,
       CueLightLiteWeb.Endpoint,
       url: [
         host: "localhost"
       ],
       secret_key_base: "psv99p19tWyRs3fGFRHRnT09WwkgfRe9ofD0SZlkgoMHYk48kTP3JX6JXrztGGtY",
       render_errors: [
         view: CueLightLiteWeb.ErrorView,
         accepts: ~w(html json),
         layout: false
       ],
       pubsub_server: CueLightLite.PubSub,
       live_view: [
         signing_salt: "iTITIm7U"
       ],
       static_url: [
         path: "/assets"
       ]

# Configures Elixir's Logger
config :logger,
       :console,
       format: "$time $metadata[$level] $message\n",
       metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# config :mnesia, :dir, '/path/to/dir'

config :cue_light_lite,
       :pow,
       user: CueLightLite.Users.User,
       repo: CueLightLite.Repo,
       routes_backend: CueLightLiteWeb.Pow.Routes,
       web_module: CueLightLiteWeb,
       cache_store_backend: Pow.Store.Backend.MnesiaCache

config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

# , default_locale: "en_US"
config :gettext, plural_forms: CueLightLiteWeb.Plural

config :libcluster,
  topologies: [
    gossip: [
      strategy: Elixir.Cluster.Strategy.Gossip,
      config: [
        port: 45892,
        if_addr: "0.0.0.0",
        multicast_addr: "255.255.255.255",
        broadcast_only: true
      ]
    ]
  ]

config :kaffy,
  otp_app: :cue_light_lite,
  ecto_repo: CueLightLite.Repo,
  router: CueLightLiteWeb.Router

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
