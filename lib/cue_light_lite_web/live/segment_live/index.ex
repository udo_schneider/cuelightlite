defmodule CueLightLiteWeb.SegmentLive.Index do
  @moduledoc false

  use CueLightLiteWeb, :live_view

  require Logger

  alias CueLightLite.Planning
  alias CueLightLite.Planning.Segment
  alias CueLightLite.RundownScheduler
  alias CueLightLiteWeb.Globalisation
  alias CueLightLiteWeb.SegmentLive.{FormComponent, RowComponent}

  @impl true
  def mount(
        %{"rundown_id" => rundown_id} = _params,
        %{"locale" => locale, "time_zone" => time_zone} = _session,
        socket
      ) do
    Globalisation.put_locale(locale)

    if connected?(socket) do
      Planning.subscribe()
      RundownScheduler.subscribe()
    end

    rundown = Planning.get_rundown_with_event!(rundown_id)
    rundown_id = rundown.id

    {
      :ok,
      socket
      |> assign(:event, rundown.event)
      |> assign(:rundown, rundown)
      |> assign(:time_zone, time_zone)
      |> assign(:on_air, RundownScheduler.started?(rundown_id))
      |> assign(:show_controls, false)
      |> assign(:time, DateTime.utc_now())
      |> assign(:running_time, 0)
      |> assign(:over_under_time, DateTime.utc_now())
      |> assign(:over_under_diff, 0)
      |> assign(:current_segment_id, RundownScheduler.current_segment_id(rundown_id))
      |> assign_list_segments()
    }
  end

  @impl true
  def mount(%{"id" => id} = params, session, socket) do
    rundown_id = Planning.get_segment_with_rundown!(id).rundown.id
    mount(Map.put(params, "rundown_id", rundown_id), session, socket)
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, dgettext("segments", "Edit Segment"))
    |> assign(:segment, Planning.get_segment!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, dgettext("segments", "New Segment"))
    |> assign(:segment, %Segment{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, dgettext("segments", "Listing Segments"))
    |> assign(:segment, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    segment = Planning.get_segment!(id)
    {:ok, _} = Planning.delete_segment(segment, socket.assigns.rundown)

    {
      :noreply,
      socket
      |> put_flash(:info, dgettext("segments", "Segment deleted successfully"))
      |> assign_list_segments()
    }
  end

  @impl true
  def handle_event("to-top", %{"id" => id}, socket) do
    Planning.reorder_segment_to_top(socket.assigns.rundown, id)
    {:noreply, assign_list_segments(socket)}
  end

  @impl true
  def handle_event("up", %{"id" => id}, socket) do
    Planning.reorder_segment_up(socket.assigns.rundown, id)
    {:noreply, assign_list_segments(socket)}
  end

  @impl true
  def handle_event("down", %{"id" => id}, socket) do
    Planning.reorder_segment_down(socket.assigns.rundown, id)
    {:noreply, assign_list_segments(socket)}
  end

  @impl true
  def handle_event("to-bottom", %{"id" => id}, socket) do
    Planning.reorder_segment_to_bottom(socket.assigns.rundown, id)
    {:noreply, assign_list_segments(socket)}
  end

  @impl true
  def handle_event("go_live", _params, socket) do
    RundownScheduler.toggle(socket.assigns.rundown.id)
    {:noreply, socket}
  end

  @impl true
  def handle_event("next", _params, socket) do
    if socket.assigns.current_segment_id,
      do: RundownScheduler.next_segment(socket.assigns.rundown.id)

    {:noreply, socket}
  end

  @impl true
  def handle_event("restart", _params, socket) do
    RundownScheduler.restart_rundown(socket.assigns.rundown.id)
    {:noreply, socket}
  end

  @impl true
  def handle_event("clean", _params, socket) do
    Planning.update_segment_fields(socket.assigns.segments, socket.assigns.rundown, true)
    {:noreply, socket}
  end

  @impl true
  def handle_event("show_controls", %{"checked" => checked}, socket) do
    {:noreply, socket |> assign(:show_controls, checked == "true")}
  end

  @impl true
  def handle_info({{:event, :created}, _event}, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_info({{:event, :updated}, event}, socket) do
    if event.id == socket.assigns.event.id do
      {
        :noreply,
        socket
        |> assign(:event, event)
      }
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info({{:event, :deleted}, event}, socket) do
    if event.id == socket.assigns.event.id do
      {
        :noreply,
        socket
        |> redirect(to: Routes.event_index_path(socket, :index))
      }
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info({{:rundown, :created}, _rundown}, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_info({{:rundown, :updated}, rundown}, socket) do
    if rundown.id == socket.assigns.rundown.id do
      {
        :noreply,
        socket
        |> assign(:rundown, rundown)
        |> assign(:segments, list_segments(rundown))
      }
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info({{:rundown, :deleted}, rundown}, socket) do
    if rundown.id == socket.assigns.rundown.id do
      {
        :noreply,
        socket
        |> redirect(to: Routes.rundown_index_path(socket, :index, socket.assigns.event))
      }
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info({{:segment, :updated}, updated_segment}, socket) do
    segments =
      socket.assigns.segments
      |> Enum.map(&if &1.id == updated_segment.id, do: updated_segment, else: &1)
      |> Enum.sort(&(&1.position < &2.position))

    {:noreply, assign(socket, :segments, segments)}
  end

  @impl true
  def handle_info({{:segment, _}, _segment}, socket) do
    {:noreply, assign(socket, :segments, list_segments(socket.assigns.rundown))}
  end

  @impl true
  def handle_info({{:scheduler, rundown_id}, details}, socket) do
    if socket.assigns.rundown.id == rundown_id do
      handle_scheduler_event(details, socket)
    else
      {:noreply, socket}
    end
  end

  defp handle_scheduler_event(:started, socket) do
    {:noreply, socket |> assign(:on_air, true)}
  end

  defp handle_scheduler_event(:stopped, socket) do
    {:noreply, socket |> assign(:on_air, false) |> assign(:current_segment_id, nil)}
  end

  defp handle_scheduler_event(
         {:tick,
          %{
            time: time,
            running_time: running_time,
            over_under_time: over_under_time,
            over_under_diff: over_under_diff
          }},
         socket
       ) do
    {:noreply,
     socket
     |> assign(:time, time)
     |> assign(:running_time, running_time)
     |> assign(:over_under_time, over_under_time)
     |> assign(:over_under_diff, over_under_diff)}
  end

  defp handle_scheduler_event(
         %{
           current_segment_id: current_segment_id,
           time: time,
           running_time: running_time,
           over_under_time: over_under_time,
           over_under_diff: over_under_diff
         },
         socket
       ) do
    {:noreply,
     socket
     |> assign(:current_segment_id, current_segment_id)
     |> assign(:time, time)
     |> assign(:running_time, running_time)
     |> assign(:over_under_time, over_under_time)
     |> assign(:over_under_diff, over_under_diff)}
  end

  defp list_segments(rundown) do
    Planning.list_segments(rundown)
  end

  defp assign_list_segments(socket) do
    segments = list_segments(socket.assigns.rundown)

    socket
    |> assign(:segments, segments)
    |> assign(:first_segment, List.first(segments))
    |> assign(:last_segment, List.last(segments))
  end
end
