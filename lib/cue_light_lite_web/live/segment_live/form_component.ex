defmodule CueLightLiteWeb.SegmentLive.FormComponent do
  @moduledoc false

  use CueLightLiteWeb, :live_component

  alias CueLightLite.Planning

  @impl true
  def update(%{segment: segment} = assigns, socket) do
    changeset = Planning.change_segment(segment)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"segment" => segment_params}, socket) do
    changeset =
      socket.assigns.segment
      |> Planning.change_segment(segment_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"segment" => segment_params}, socket) do
    save_segment(socket, socket.assigns.action, segment_params)
  end

  defp save_segment(socket, :edit, segment_params) do
    case Planning.update_segment(
           Planning.change_segment(socket.assigns.segment, segment_params),
           socket.assigns.rundown
         ) do
      {:ok, _segment} ->
        {:noreply,
         socket
         |> put_flash(:info, dgettext("segments", "Segment updated successfully"))
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_segment(socket, :new, segment_params) do
    case Planning.create_segment(socket.assigns.rundown, segment_params) do
      {:ok, _segment} ->
        {:noreply,
         socket
         |> put_flash(:info, dgettext("segments", "Segment created successfully"))
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
