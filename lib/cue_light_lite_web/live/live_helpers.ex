defmodule CueLightLiteWeb.LiveHelpers do
  @moduledoc false
  import Phoenix.LiveView.Helpers

  @doc """
  Renders a component inside the `CueLightLiteWeb.ModalComponent` component.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <%= live_modal @socket, CueLightLiteWeb.SegmentLive.FormComponent,
        id: @segment.id || :new,
        action: @live_action,
        segment: @segment,
        return_to: Routes.segment_index_path(@socket, :index) %>
  """
  def live_modal(socket, component, opts) do
    path = Keyword.fetch!(opts, :return_to)
    modal_opts = [id: :modal, return_to: path, component: component, opts: opts]
    live_component(socket, CueLightLiteWeb.ModalComponent, modal_opts)
  end

  def live_modal_card(socket, component, opts) do
    path = Keyword.fetch!(opts, :return_to)
    title = Keyword.fetch!(opts, :title)
    modal_opts = [id: :modal, return_to: path, component: component, opts: opts, title: title]
    live_component(socket, CueLightLiteWeb.ModalComponentCard, modal_opts)
  end
end
