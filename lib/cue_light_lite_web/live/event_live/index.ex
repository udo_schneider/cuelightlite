defmodule CueLightLiteWeb.EventLive.Index do
  @moduledoc false

  use CueLightLiteWeb, :live_view

  alias CueLightLite.Planning
  alias CueLightLite.Planning.Event
  alias CueLightLiteWeb.Globalisation

  @impl true
  def mount(_params, %{"locale" => locale} = _session, socket) do
    Globalisation.put_locale(locale)
    if connected?(socket), do: Planning.subscribe()
    {:ok, assign(socket, :events, list_events())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, dgettext("events", "Edit Event"))
    |> assign(:event, Planning.get_event!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, dgettext("events", "New Event"))
    |> assign(:event, %Event{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, dgettext("events", "My Events"))
    |> assign(:event, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    event = Planning.get_event!(id)
    {:ok, _} = Planning.delete_event(event)

    {:noreply,
     socket
     |> put_flash(:info, dgettext("events", "Event deleted successfully"))
     |> assign(:events, list_events())}
  end

  @impl true
  def handle_info({{:event, _}, _event}, socket) do
    {:noreply, assign(socket, :events, list_events())}
  end

  @impl true
  def handle_info({{:rundown, _}, _rundown}, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_info({{:segment, _}, _segment}, socket) do
    {:noreply, socket}
  end

  defp list_events do
    Planning.list_events()
  end
end
