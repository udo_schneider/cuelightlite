defmodule CueLightLiteWeb.ModalComponentCard do
  @moduledoc false
  use CueLightLiteWeb, :live_component

  @impl true
  def render(assigns) do
    ~L"""
    <div id="<%= @id %>" class="modal is-active"
      phx-capture-click="close"
      phx-window-keydown="close"
      phx-key="escape"
      phx-target="#<%= @id %>"
      phx-page-loading>
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title"><%= assigns.title %></p>
          <%= live_patch to: @return_to, class: "phx-modal-close" do %>
          <button class="delete" aria-label="close"></button>
          <% end %>
        </header>
        <%= live_component @socket, @component, @opts %>
      </div>
    </div>
    """
  end

  @impl true
  def handle_event("close", _, socket) do
    {:noreply, push_patch(socket, to: socket.assigns.return_to)}
  end

  #           <%= live_patch to: @return_to, class: "phx-modal-close" do %>
  #          <button class="delete" aria-label="close"></button>
  #          <% end %>
end
