defmodule CueLightLiteWeb.RundownLive.Index do
  @moduledoc false

  use CueLightLiteWeb, :live_view

  alias CueLightLite.Planning
  alias CueLightLite.Planning.Rundown
  alias CueLightLiteWeb.Globalisation

  @impl true
  def mount(
        %{"event_id" => event_id} = _params,
        %{"locale" => locale, "time_zone" => time_zone} = _session,
        socket
      ) do
    Globalisation.put_locale(locale)
    if connected?(socket), do: Planning.subscribe()
    event = Planning.get_event!(event_id)

    {:ok,
     socket
     |> assign(:event, event)
     |> assign(:rundowns, list_rundowns(event))
     |> assign(:time_zone, time_zone)}
  end

  @impl true
  def mount(%{"id" => id} = params, session, socket) do
    event_id = Planning.get_rundown_with_event!(id).event.id
    mount(Map.put(params, "event_id", event_id), session, socket)
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, dgettext("rundowns", "Edit Rundown"))
    |> assign(:rundown, Planning.get_rundown!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, dgettext("rundowns", "New Rundown"))
    |> assign(:rundown, %Rundown{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, dgettext("rundowns", "Listing Rundowns"))
    |> assign(:rundown, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    rundown = Planning.get_rundown_with_event!(id)
    {:ok, _} = Planning.delete_rundown(rundown)

    {:noreply,
     socket
     |> put_flash(:info, dgettext("rundowns", "Rundown deleted successfully"))
     |> assign(:rundowns, list_rundowns(socket.assigns.event))}
  end

  @impl true
  def handle_info({{:event, :created}, _event}, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_info({{:event, :updated}, event}, socket) do
    if event.id == socket.assigns.event.id do
      {:noreply, socket |> assign(:event, event)}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info({{:event, :deleted}, event}, socket) do
    if event.id == socket.assigns.event.id do
      {:noreply, socket |> redirect(to: Routes.event_index_path(socket, :index))}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info({{:rundown, _}, _rundown}, socket) do
    {:noreply, assign(socket, :rundowns, list_rundowns(socket.assigns.event))}
  end

  @impl true
  def handle_info({{:segment, _}, _segment}, socket) do
    {:noreply, socket}
  end

  defp list_rundowns(event) do
    Planning.list_rundowns(event)
  end
end
