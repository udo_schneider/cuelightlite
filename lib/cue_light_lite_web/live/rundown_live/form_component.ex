defmodule CueLightLiteWeb.RundownLive.FormComponent do
  @moduledoc false

  use CueLightLiteWeb, :live_component

  alias CueLightLite.Planning
  import CueLightLiteWeb.BulmaHelpers

  @impl true
  def update(%{rundown: rundown} = assigns, socket) do
    changeset = Planning.change_rundown(rundown)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"rundown" => rundown_params}, socket) do
    changeset =
      socket.assigns.rundown
      |> Planning.change_rundown(rundown_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"rundown" => rundown_params}, socket) do
    save_rundown(socket, socket.assigns.action, rundown_params)
  end

  defp save_rundown(socket, :edit, rundown_params) do
    case Planning.update_rundown(socket.assigns.rundown, rundown_params) do
      {:ok, _rundown} ->
        {:noreply,
         socket
         |> put_flash(:info, dgettext("rundowns", "Rundown updated successfully"))
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_rundown(socket, :new, rundown_params) do
    case Planning.create_rundown(socket.assigns.event, rundown_params) do
      {:ok, _rundown} ->
        {:noreply,
         socket
         |> put_flash(:info, dgettext("rundowns", "Rundown created successfully"))
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
