defmodule CueLightLiteWeb.RundownLive.Show do
  @moduledoc false

  use CueLightLiteWeb, :live_view

  alias CueLightLite.Planning

  @impl true
  def mount(%{"id" => id} = _params, %{"time_zone" => time_zone} = _session, socket) do
    rundown = Planning.get_rundown!(id)

    {:ok,
     socket
     |> assign(:event, rundown.event)
     |> assign(:rundown, rundown)
     |> assign(:time_zone, time_zone)}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    rundown = Planning.get_rundown_with_event!(id)

    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:event, rundown.event)
     |> assign(:rundown, rundown)}
  end

  defp page_title(:show), do: "Show Rundown"
  defp page_title(:edit), do: "Edit Rundown"
end
