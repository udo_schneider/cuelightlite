defmodule CueLightLiteWeb.Router do
  use CueLightLiteWeb, :router
  use Pow.Phoenix.Router

  require CueLightLiteWeb.{Cldr, Gettext}

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :set_user_locale_timezone

    plug Cldr.Plug.SetLocale,
      apps: [cldr: CueLightLiteWeb.Cldr, gettext: CueLightLiteWeb.Gettext, gettext: :global]

    # plug :introspect
    plug :fetch_live_flash
    plug :put_root_layout, {CueLightLiteWeb.LayoutView, :root}
    plug :protect_from_forgery

    plug :put_secure_browser_headers,
         %{
           "content-security-policy" => "default-src 'self'; \
                          script-src 'self' 'unsafe-eval' 'unsafe-inline'; \
                          connect-src 'self' wss: ws:; \
                          style-src 'self' 'unsafe-inline' cdn.jsdelivr.net; \
                          font-src 'self' data: cdn.jsdelivr.net; \
                          img-src 'self' data: ;"
         }

    plug CueLightLiteWeb.Plugs.SetLocale
  end

  pipeline :require_authenticated do
    plug Pow.Plug.RequireAuthenticated, error_handler: Pow.Phoenix.PlugErrorHandler
  end

  pipeline :admin_authenticated do
    plug :admin_auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through [:browser]

    pow_routes()
  end

  scope "/", CueLightLiteWeb do
    pipe_through [:browser]

    live "/", PageLive, :index
  end

  scope "/", CueLightLiteWeb do
    pipe_through [:browser, :require_authenticated]

    live "/events", EventLive.Index, :index
    live "/events/new", EventLive.Index, :new
    live "/events/:id/edit", EventLive.Index, :edit
    live "/events/:id", EventLive.Show, :show
    live "/events/:id/show/edit", EventLive.Show, :edit

    live "/events/:event_id/rundowns", RundownLive.Index, :index
    live "/events/:event_id/rundowns/new", RundownLive.Index, :new
    live "/rundowns/:id/edit", RundownLive.Index, :edit
    live "/rundowns/:id", RundownLive.Show, :show
    live "/rundowns/:id/show/edit", RundownLive.Show, :edit

    live "/rundowns/:rundown_id/segments", SegmentLive.Index, :index
    live "/rundowns/:rundown_id/segments/new", SegmentLive.Index, :new
    live "/segments/:id/edit", SegmentLive.Index, :edit
    live "/segments/:id", SegmentLive.Show, :show
    live "/segments/:id/show/edit", SegmentLive.Show, :edit
  end

  # Other scopes may use custom stacks.
  # scope "/api", CueLightLiteWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    use Kaffy.Routes, scope: "/admin", pipe_through: []

    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard",
        metrics: CueLightLiteWeb.Telemetry,
        ecto_repos: CueLightLite.Repo
    end
  else
    use Kaffy.Routes, scope: "/admin", pipe_through: [:admin_authenticated]

    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:browser, :admin_authenticated]

      live_dashboard "/dashboard",
        metrics: CueLightLiteWeb.Telemetry,
        ecto_repos: CueLightLite.Repo
    end
  end

  defp admin_auth(conn, _opts) do
    username = System.fetch_env!("ADMIN_USERNAME")
    password = System.fetch_env!("ADMIN_PASSWORD")
    Plug.BasicAuth.basic_auth(conn, username: username, password: password)
  end

  def introspect(conn, _opts) do
    IO.puts("""
    Private: #{inspect(conn.private)}
    """)

    conn
  end

  def set_user_locale_timezone(conn, _opts) do
    alias CueLightLite.Users.User

    case conn.assigns.current_user do
      nil ->
        conn

      current_user ->
        conn
        |> put_session(:time_zone, User.time_zone(current_user))
        |> put_session(:locale, User.locale(current_user))
    end
  end
end
