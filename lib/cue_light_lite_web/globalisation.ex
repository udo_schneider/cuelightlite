defmodule CueLightLiteWeb.Globalisation do
  @moduledoc false

  alias CueLightLite.Duration

  alias CueLightLiteWeb.Cldr
  alias CueLightLiteWeb.Cldr.Locale

  def localize_datetime(nil, _), do: nil
  def localize_datetime(%DateTime{} = datetime, nil), do: DateTime.shift_zone!(datetime, "UTC")

  def localize_datetime(%DateTime{} = datetime, time_zone),
    do: DateTime.shift_zone!(datetime, time_zone)

  def cldr_datetime_string(%DateTime{} = datetime),
    do: CueLightLiteWeb.Cldr.DateTime.to_string!(datetime)

  def cldr_time_string(%DateTime{} = datetime),
    do: datetime |> DateTime.to_time() |> cldr_time_string()

  def cldr_time_string(%Time{} = time),
    do: CueLightLiteWeb.Cldr.Time.to_string!(time)

  def cldr_localized_datetime_string(nil, _time_zone), do: ""

  def cldr_localized_datetime_string(%DateTime{} = datetime, time_zone),
    do: datetime |> localize_datetime(time_zone) |> cldr_datetime_string()

  def cldr_localized_time_string(nil, _time_zone), do: ""

  def cldr_localized_time_string(%DateTime{} = datetime, time_zone),
    do: datetime |> localize_datetime(time_zone) |> cldr_time_string()

  def put_locale(locale) do
    gettext_locale_name = Locale.new!(locale).gettext_locale_name
    Gettext.put_locale(CueLightLiteWeb.Gettext, gettext_locale_name)
    Gettext.put_locale(gettext_locale_name)
    Cldr.put_locale(locale)
  end

  def localized_duration_string(nil), do: ""

  def localized_duration_string(seconds) when is_integer(seconds),
    do: localized_duration_string(%Duration{seconds: seconds})

  def localized_duration_string(%Duration{} = duration), do: to_string(duration)

  def localized_duration_signed_string(nil), do: ""

  def localized_duration_signed_string(seconds) when is_integer(seconds),
    do: localized_duration_signed_string(%Duration{seconds: seconds})

  def localized_duration_signed_string(%Duration{seconds: seconds} = duration) do
    if seconds > 0, do: ["+", to_string(duration)], else: to_string(duration)
  end
end
