defmodule CueLightLiteWeb.Plural do
  @moduledoc false

  @behaviour Gettext.Plural

  def nplurals("llp"), do: 3
  def nplurals(locale), do: Gettext.Plural.nplurals(locale)

  def plural("llp", 0), do: 0
  def plural("llp", 1), do: 1
  def plural("llp", _), do: 2
  def plural(locale, n), do: Gettext.Plural.plural(locale, n)
end
