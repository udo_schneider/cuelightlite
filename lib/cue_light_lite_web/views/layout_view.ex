defmodule CueLightLiteWeb.LayoutView do
  use CueLightLiteWeb, :view
  require Logger

  defp full_name(%{given_name: nil, family_name: nil}), do: "Unknown"
  defp full_name(%{given_name: given_name, family_name: nil}), do: given_name
  defp full_name(%{given_name: nil, family_name: family_name}), do: family_name

  defp full_name(%{given_name: given_name, family_name: family_name}),
    do: [family_name, ", ", given_name]

  defp nav_link(conn, text, opts)

  defp nav_link(conn, opts, do: contents) when is_list(opts) do
    nav_link(conn, contents, opts)
  end

  defp nav_link(conn, text, opts) when is_list(opts) do
    to = Keyword.get(opts, :to)
    is_active = to == Phoenix.Controller.current_path(conn)

    {_, opts} =
      Keyword.get_and_update(
        opts,
        :class,
        fn current ->
          case {current, is_active} do
            {nil, false} -> {nil, nil}
            {nil, true} -> "is-active"
            {class, false} -> {class, class}
            {class, true} -> {class, class <> " is-active"}
          end
        end
      )

    link(text, opts)
  end
end
