defmodule CueLightLiteWeb.BulmaHelpers do
  @moduledoc false

  use Phoenix.HTML

  alias CueLightLiteWeb.Globalisation

  def bulma_datetime_select(form, field, opts \\ []) do
    {time_zone, opts} = Keyword.pop(opts, :time_zone)
    value = Globalisation.localize_datetime(input_value(form, field), time_zone)

    builder = fn b ->
      ~e"""
      <div class="select"><%= b.(:day, []) %></div>
      <div class="select"><%= b.(:month, []) %></div>
      <div class="select"><%= b.(:year, []) %></div>
      <div class="select"><%= b.(:hour, []) %></div>
      <div class="select"><%= b.(:minute, []) %></div>
      """
    end

    [
      datetime_select(form, field, [builder: builder, value: value] ++ opts),
      hidden_input(form, field,
        name: form |> input_name(field) |> input_name("time_zone"),
        value: time_zone
      )
    ]
  end
end
