defmodule CueLightLiteWeb.IconHelpers do
  @moduledoc false

  use Phoenix.HTML

  def mdi(name, options \\ "") do
    ~E|<span class="icon <%= options %>"><i class="mdi mdi-18px <%= name %>"></i></span>|
  end

  def mdi_text(name, text, options \\ "") do
    ~E|<span class="icon-text <%= options %>"><%= mdi(name) %><span><%= text %></span></span>|
  end
end
