defmodule CueLightLiteWeb.Pow.Routes do
  @moduledoc false

  use Pow.Phoenix.Routes

  alias CueLightLiteWeb.Router.Helpers, as: Routes

  @impl true
  def after_sign_out_path(conn), do: Routes.page_path(conn, :index)
end
