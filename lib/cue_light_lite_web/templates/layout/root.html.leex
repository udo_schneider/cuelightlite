<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <%= csrf_meta_tag() %>
    <%= live_title_tag assigns[:page_title] || "Home", suffix: " · CueLightLite" %>
    <link phx-track-static rel="stylesheet" href="<%= Routes.static_path(@conn, "/css/app.css") %>"/>
    <script defer phx-track-static type="text/javascript" src="<%= Routes.static_path(@conn, "/js/app.js") %>"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mdi/font@5.9.55/css/materialdesignicons.min.css">
    <link rel="apple-touch-icon" sizes="180x180" href="<%= Routes.static_path(@conn, "/favicon/apple-touch-icon.png") %>">
    <link rel="icon" type="image/png" sizes="32x32" href="<%= Routes.static_path(@conn, "/favicon/favicon-32x32.png") %>">
    <link rel="icon" type="image/png" sizes="16x16" href="<%= Routes.static_path(@conn, "/favicon/favicon-16x16.png") %>">
    <link rel="manifest" href="<%= Routes.static_path(@conn, "/favicon/site.webmanifest") %>">
    <link rel="mask-icon" href="<%= Routes.static_path(@conn, "/favicon/safari-pinned-tab.svg") %>" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="CueLightLite">
    <meta name="application-name" content="CueLightLite">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">
</head>
<body class="has-navbar-fixed-top">

<nav class="navbar has-shadow is-fixed-top is-light" role="navigation" aria-label="main navigation">

    <div class="navbar-brand">
        <%= nav_link @conn, to: Routes.page_path(@conn, :index), class: "navbar-item" do %>
        <img src="<%= Routes.static_path(CueLightLiteWeb.Endpoint, "/images/logo.svg") %>" width="28" height="28" alt="<%= "#{Node.self()}" %>">
        <% end %>
        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false"
           data-target="navMenu">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div class="navbar-menu" id="navMenu">
        <div class="navbar-start">
            <%= if @current_user do %>
            <%= nav_link @conn, to: Routes.event_index_path(@conn, :index), class: "navbar-item" do %>
            <span><%= dgettext("events", "Events") %></span>
            <% end %>
            <% end %>
        </div>

        <div class="navbar-end">
            <%= if @current_user == nil do %>
            <div class="navbar-item">
                <div class="buttons">
                    <%= link to: Routes.pow_registration_path(@conn, :new) , class: "button is-primary"  do %>
                        <%= Icon.mdi_text("mdi-account-plus", dgettext("accounts", "Sign up")) %>
                    <% end %>
                    <%= link to: Routes.pow_session_path(@conn, :new), class: "button is-link"  do %>
                        <%= Icon.mdi_text("mdi-login", dgettext("accounts", "Login")) %>
                    <% end %>
                </div>
            </div>
            <% else %>
            <div class="navbar-item has-dropdown is-hoverable is-right">
                <a class="navbar-link">
                    <%= Icon.mdi_text("mdi-account", full_name(@current_user)) %>
                </a>

                <div class="navbar-dropdown">
                    <%= link  class: "navbar-item", to: Routes.pow_registration_path(@conn, :edit)  do %>
                    <%= Icon.mdi_text("mdi-account-edit", dgettext("accounts", "Account")) %>
                    <% end %>
                    <%= link class: "navbar-item", to: Routes.pow_session_path(@conn, :delete), method: :delete do  %>
                    <%= Icon.mdi_text("mdi-logout", dgettext("accounts", "Sign out")) %>
                    <% end %>
                </div>
            </div>
            <% end %>
        </div>
    </div>
</nav>
<%= @inner_content %>
</body>
</html>
