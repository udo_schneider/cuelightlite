defmodule CueLightLiteWeb.Plugs.SetLocale do
  @moduledoc false

  import Plug.Conn

  def init(_options), do: nil

  def call(conn, _options) do
    case fetch_locale_from(conn) do
      nil ->
        conn

      locale ->
        CueLightLiteWeb.Globalisation.put_locale(locale)

        conn
    end
  end

  defp fetch_locale_from(conn), do: get_session(conn, "locale")
end
