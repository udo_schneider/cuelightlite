defmodule CueLightLite.RundownScheduler.NodeListener do
  @moduledoc false

  use GenServer

  alias CueLightLite.{
    RundownSchedulerHandoff,
    RundownSchedulerRegistry,
    RundownSchedulerSupervisor
  }

  def start_link(_), do: GenServer.start_link(__MODULE__, [])

  def init(_) do
    :net_kernel.monitor_nodes(true, node_type: :visible)
    {:ok, nil}
  end

  def handle_info({:nodeup, _node, _node_type}, state) do
    set_members(RundownSchedulerRegistry)
    set_members(RundownSchedulerSupervisor)
    set_neighbours(RundownSchedulerHandoff)
    {:noreply, state}
  end

  def handle_info({:nodedown, _node, _node_type}, state) do
    set_members(RundownSchedulerRegistry)
    set_members(RundownSchedulerSupervisor)
    set_neighbours(RundownSchedulerHandoff)
    {:noreply, state}
  end

  defp set_members(name) do
    members =
      [Node.self() | Node.list()]
      |> Enum.map(fn node -> {name, node} end)

    :ok = Horde.Cluster.set_members(name, members)
  end

  defp set_neighbours(name) do
    neighbours =
      Node.list()
      |> Enum.map(fn node -> {name, node} end)

    DeltaCrdt.set_neighbours(name, neighbours)
  end
end
