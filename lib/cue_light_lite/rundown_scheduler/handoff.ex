defmodule CueLightLite.RundownScheduler.Handoff do
  @moduledoc false

  use GenServer
  require Logger

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def child_spec(opts \\ []) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]}
    }
  end

  def handoff(rundown_id, current_segment_id) do
    GenServer.call(__MODULE__, {:handoff, rundown_id, current_segment_id})
  end

  def pickup(rundown_id) do
    GenServer.call(__MODULE__, {:pickup, rundown_id})
  end

  def remove(rundown_id) do
    GenServer.call(__MODULE__, {:remove, rundown_id})
  end

  def init(_) do
    # custom config for aggressive CRDT sync
    {:ok, crdt_pid} =
      DeltaCrdt.start_link(DeltaCrdt.AWLWWMap,
        sync_interval: 5,
        ship_interval: 5,
        ship_debounce: 1
      )

    {:ok, crdt_pid}
  end

  def handle_call({:handoff, rundown_id, current_segment_id}, _from, crdt_pid) do
    DeltaCrdt.mutate(crdt_pid, :add, [rundown_id, current_segment_id])

    Logger.warn("Added Rundown #{rundown_id} current segment #{current_segment_id} to CRDT")

    Logger.warn("CRDT: #{inspect(DeltaCrdt.read(crdt_pid))}")
    {:reply, :ok, crdt_pid}
  end

  def handle_call({:pickup, rundown_id}, _from, crdt_pid) do
    current_segment_id =
      crdt_pid
      |> DeltaCrdt.read()
      |> Map.get(rundown_id, nil)

    Logger.warn("CRDT: #{inspect(DeltaCrdt.read(crdt_pid))}")

    Logger.warn(
      "Picked up current segment #{current_segment_id} for Rundown #{rundown_id}from CRDT"
    )

    DeltaCrdt.mutate(crdt_pid, :remove, [rundown_id])

    {:reply, current_segment_id, crdt_pid}
  end

  def handle_call({:remove, rundown_id}, _from, crdt_pid) do
    Logger.warn("CRDT: #{inspect(DeltaCrdt.read(crdt_pid))}")
    Logger.warn("Remove current segment for Rundown #{rundown_id}from CRDT")
    DeltaCrdt.mutate(crdt_pid, :remove, [rundown_id])

    {:reply, :ok, crdt_pid}
  end
end
