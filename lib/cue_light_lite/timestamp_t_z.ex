defmodule CueLightLite.TimestampTZ do
  @moduledoc false

  use Ecto.Type
  def type, do: :utc_datetime

  def cast(%{"time_zone" => time_zone} = timestamp) when is_binary(time_zone),
    do: utc_timestamp(timestamp, time_zone)

  def cast(%{time_zone: time_zone} = timestamp) when is_binary(time_zone),
    do: utc_timestamp(timestamp, time_zone)

  def cast(timestamp), do: Ecto.Type.cast(:utc_datetime, timestamp)

  def load(data), do: Ecto.Type.load(:utc_datetime, data)

  def dump(timestamp) do
    case DateTime.shift_zone(timestamp, "Etc/UTC") do
      {:ok, utc_datetime} -> Ecto.Type.dump(:utc_datetime, utc_datetime)
      _ -> :error
    end
  end

  defp utc_timestamp(timestamp, time_zone) when is_binary(time_zone) do
    with {:ok, naive} <- Ecto.Type.cast(:naive_datetime, timestamp),
         {:ok, datetime} <- DateTime.from_naive(naive, time_zone) do
      {:ok, datetime}
    else
      _ -> :error
    end
  end
end
