defmodule CueLightLite.Planning.Event do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  alias CueLightLite.Planning.Rundown

  @timestamps_opts [type: :utc_datetime]

  schema "events" do
    field :title, :string
    field :description, :string
    has_many :rundowns, Rundown

    timestamps()
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, [:title, :description])
    |> validate_required([:title])
  end
end
