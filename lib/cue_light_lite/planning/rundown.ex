defmodule CueLightLite.Planning.Rundown do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias CueLightLite.Planning.{Event, Segment}
  alias CueLightLite.TimestampTZ

  @timestamps_opts [type: :utc_datetime]

  schema "rundowns" do
    field :title, :string
    field :start, TimestampTZ
    field :finish, TimestampTZ

    belongs_to :event, Event
    has_many :segments, Segment

    timestamps()
  end

  @doc false
  def changeset(rundown, attrs) do
    rundown
    |> cast(attrs, [:title, :start, :finish])
    |> validate_required([:title, :start, :finish])
    |> validate_start_before_finish()
  end

  defp validate_start_before_finish(%{valid?: true} = changeset) do
    {_, start} = fetch_field(changeset, :start)
    {_, finish} = fetch_field(changeset, :finish)

    case NaiveDateTime.compare(start, finish) do
      :lt -> changeset
      _ -> add_error(changeset, :finish, "must be later than start")
    end
  end

  defp validate_start_before_finish(changeset), do: changeset
end
