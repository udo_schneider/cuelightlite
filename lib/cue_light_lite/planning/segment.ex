defmodule CueLightLite.Planning.Segment do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias CueLightLite.Duration
  alias CueLightLite.Planning
  alias CueLightLite.Planning.Rundown

  @timestamps_opts [type: :utc_datetime]

  schema "segments" do
    field :position, :integer
    field :title, :string
    field :planned_start, :utc_datetime
    field :planned_duration, Duration
    field :planned_finish, :utc_datetime

    field :estimated_start, :utc_datetime
    field :estimated_finish, :utc_datetime

    field :actual_start, :utc_datetime
    field :actual_duration, Duration
    field :actual_finish, :utc_datetime

    field :notes, :string

    belongs_to :rundown, Rundown

    timestamps()
  end

  @doc false
  def form_changeset(segment, attrs \\ %{}) do
    segment
    |> cast(attrs, [:position, :title, :planned_duration, :notes])
    |> validate_required([:position, :title, :planned_duration])
  end

  @doc false
  def full_changeset(segment, attrs \\ %{}) do
    segment
    |> cast(attrs, [
      :position,
      :title,
      :planned_start,
      :planned_duration,
      :planned_finish,
      :estimated_start,
      :estimated_finish,
      :actual_start,
      :actual_duration,
      :actual_finish,
      :notes
    ])
    |> validate_required([:position, :title, :planned_duration])
  end

  @doc false
  def create_changeset(segment, attrs \\ %{}, rundown) do
    segment
    |> cast(attrs, [:title, :planned_start, :planned_duration, :planned_finish, :notes])
    |> Ecto.Changeset.put_assoc(:rundown, rundown)
    |> put_new_position()
    |> validate_required([:position, :title, :planned_duration])
  end

  defp put_new_position(%Ecto.Changeset{} = changeset) do
    case Ecto.Changeset.get_field(changeset, :position) do
      nil -> Ecto.Changeset.put_change(changeset, :position, next_segment_position(changeset))
      _ -> changeset
    end
  end

  defp next_segment_position(%Ecto.Changeset{} = changeset) do
    last_segment =
      changeset
      |> Ecto.Changeset.get_field(:rundown)
      |> Planning.get_last_segment()

    case last_segment do
      nil -> 1
      segment -> segment.position + 1
    end
  end
end
