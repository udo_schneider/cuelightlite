defmodule CueLightLite.Repo do
  use Ecto.Repo,
    otp_app: :cue_light_lite,
    adapter: Ecto.Adapters.Postgres
end
