defmodule CueLightLite.Planning do
  @moduledoc """
  The Planning context.
  """

  import Ecto.Query, warn: false
  alias CueLightLite.Repo

  alias CueLightLite.Planning.{Event, Rundown, Segment}

  def list_events do
    from(e in Event, order_by: [asc: e.title])
    |> Repo.all()
  end

  def get_event!(id), do: Repo.get!(Event, id)

  def create_event(attrs \\ %{}) do
    %Event{}
    |> Event.changeset(attrs)
    |> Repo.insert()
    |> broadcast({:event, :created})
  end

  def update_event(%Event{} = event, attrs) do
    event
    |> Event.changeset(attrs)
    |> Repo.update()
    |> broadcast({:event, :updated})
  end

  def delete_event(%Event{} = event) do
    Repo.delete(event)
    |> broadcast({:event, :deleted})
  end

  def change_event(%Event{} = event, attrs \\ %{}) do
    Event.changeset(event, attrs)
  end

  def list_rundowns(%Event{} = event) do
    from(r in Rundown, where: r.event_id == ^event.id, order_by: [asc: r.start])
    |> Repo.all()
  end

  def get_rundown!(id), do: Repo.get!(Rundown, id)

  def get_rundown_with_event!(id) do
    get_rundown!(id)
    |> Repo.preload(:event)
  end

  def create_rundown(%Event{} = event, attrs \\ %{}) do
    %Rundown{}
    |> Rundown.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:event, event)
    |> Repo.insert()
    |> broadcast({:rundown, :created})
  end

  def update_rundown(%Rundown{} = rundown, attrs) do
    rundown
    |> Rundown.changeset(attrs)
    |> Repo.update()
    |> update_segment_fields(rundown)
    |> broadcast({:rundown, :updated})
  end

  def delete_rundown(%Rundown{} = rundown) do
    Repo.delete(rundown)
    |> broadcast({:rundown, :deleted})
  end

  def change_rundown(%Rundown{} = rundown, attrs \\ %{}) do
    Rundown.changeset(rundown, attrs)
  end

  def list_segments(%Rundown{} = rundown) do
    from(s in Segment, where: s.rundown_id == ^rundown.id, order_by: [asc: s.position])
    |> Repo.all()
  end

  def get_segment!(id), do: Repo.get!(Segment, id)

  def get_segment_with_rundown!(id) do
    get_segment!(id)
    |> Repo.preload(:rundown)
  end

  def get_segment_with_rundown_and_event!(id) do
    get_segment!(id)
    |> Repo.preload(rundown: [:event])
  end

  def get_last_segment(%Rundown{} = rundown) do
    from(s in Segment, where: s.rundown_id == ^rundown.id, order_by: [desc: s.position], limit: 1)
    |> Repo.one()
  end

  def create_segment(%Rundown{} = rundown, attrs \\ %{}) do
    case %Segment{}
         |> Segment.create_changeset(attrs, rundown)
         |> Repo.insert()
         |> update_segment_fields(rundown)
         |> broadcast({:segment, :created}) do
      {:ok, segment} -> {:ok, get_segment_with_rundown_and_event!(segment.id)}
      result -> result
    end
  end

  def update_segment(%Ecto.Changeset{} = changeset, rundown) do
    changeset
    |> update_segment_basic()
    |> update_segment_fields(rundown)
  end

  defp update_segment_basic(%Ecto.Changeset{} = changeset) do
    changeset
    |> Repo.update()
    |> broadcast({:segment, :updated})
  end

  def delete_segment(%Segment{} = segment, rundown) do
    segment
    |> Repo.delete()
    |> update_segment_fields(rundown)
    |> broadcast({:segment, :deleted})
  end

  def change_segment(%Segment{} = segment, attrs \\ %{}) do
    Segment.form_changeset(segment, attrs)
  end

  def reorder_segment_to_top(rundown, id) do
    segment = get_segment!(id)

    rest =
      list_segments(rundown)
      |> Enum.filter(fn each -> each.id != segment.id end)

    update_segment_fields([segment | rest], rundown)
  end

  def reorder_segment_up(rundown, id) do
    segment = get_segment!(id)
    segments = list_segments(rundown)

    if Enum.count(segments) >= 2 and List.first(segments).id != segment.id do
      index1 = Enum.find_index(segments, fn each -> each.id == segment.id end)
      index2 = index1 - 1
      reorder_segment_swap(segments, index1, index2, rundown)
    end
  end

  def reorder_segment_down(rundown, id) do
    segment = get_segment!(id)
    segments = list_segments(rundown)

    if Enum.count(segments) >= 2 and List.last(segments).id != segment.id do
      index1 = Enum.find_index(segments, fn each -> each.id == segment.id end)
      index2 = index1 + 1
      reorder_segment_swap(segments, index1, index2, rundown)
    end
  end

  defp reorder_segment_swap(segments, index1, index2, rundown) do
    element1 = Enum.at(segments, index1)
    element2 = Enum.at(segments, index2)

    segments
    |> List.replace_at(index1, element2)
    |> List.replace_at(index2, element1)
    |> update_segment_fields(rundown)
  end

  def reorder_segment_to_bottom(rundown, id) do
    segment = get_segment!(id)

    rest =
      list_segments(rundown)
      |> Enum.filter(fn each -> each.id != segment.id end)

    update_segment_fields(rest ++ [segment], rundown)
  end

  def update_segment_fields({:error, _} = result, _rundown), do: result
  def update_segment_fields({:ok, rundown} = result, rundown), do: result

  def update_segment_fields({:ok, _} = result, rundown) do
    rundown
    |> list_segments()
    |> update_segment_fields(rundown)

    result
  end

  def update_segment_fields(segments, rundown, clear_actual \\ false) do
    rundown = get_rundown!(rundown.id)

    acc = %{
      last_position: 0,
      start: nil,
      last_planned_finish: rundown.start,
      last_estimated_finish: rundown.start
    }

    segments
    |> Enum.reduce(acc, fn segment,
                           %{
                             last_position: last_position,
                             last_planned_finish: last_planned_finish,
                             last_estimated_finish: last_estimated_finish
                           } = acc ->
      position = last_position + 1
      planned_start = last_planned_finish
      planned_finish = DateTime.add(planned_start, segment.planned_duration.seconds, :second)

      estimated_start =
        if segment.actual_start != nil and not clear_actual,
          do: segment.actual_start,
          else: last_estimated_finish

      estimated_finish = DateTime.add(estimated_start, segment.planned_duration.seconds, :second)

      attrs =
        if clear_actual do
          %{
            position: position,
            planned_start: planned_start,
            planned_finish: planned_finish,
            estimated_start: estimated_start,
            estimated_finish: estimated_finish,
            actual_start: nil,
            actual_duration: nil,
            actual_finish: nil
          }
        else
          %{
            position: position,
            planned_start: planned_start,
            planned_finish: planned_finish,
            estimated_start: estimated_start,
            estimated_finish: estimated_finish
          }
        end

      segment
      |> Segment.full_changeset(attrs)
      |> update_segment_basic()

      %{
        acc
        | last_position: position,
          start: planned_start,
          last_planned_finish: planned_finish,
          last_estimated_finish: estimated_finish
      }
    end)
  end

  def subscribe, do: Phoenix.PubSub.subscribe(CueLightLite.PubSub, "planning")

  defp broadcast({:error, _reason} = result, _event), do: result

  defp broadcast({:ok, element} = result, event) do
    Phoenix.PubSub.broadcast(CueLightLite.PubSub, "planning", {event, element})
    result
  end
end
