defmodule CueLightLite.Duration do
  @moduledoc false

  use Ecto.Type

  defstruct seconds: 0

  def type, do: :integer

  def cast(string) when is_binary(string), do: parse(string)

  def cast(integer) when is_integer(integer), do: {:ok, %__MODULE__{seconds: integer}}

  def cast(_), do: :error

  def load(integer) when is_integer(integer) do
    {:ok, %__MODULE__{seconds: integer}}
  end

  def dump(integer) when is_integer(integer), do: {:ok, integer}
  def dump(%__MODULE__{seconds: integer}) when is_integer(integer), do: {:ok, integer}
  def dump(_), do: :error

  defimpl Inspect, for: __MODULE__ do
    def inspect(duration, _opts) do
      {:ok, string} = CueLightLite.Duration.format(duration.seconds)
      string
    end
  end

  defimpl String.Chars, for: __MODULE__ do
    def to_string(duration) do
      {:ok, string} = CueLightLite.Duration.format(duration.seconds)
      string
    end
  end

  defimpl Phoenix.HTML.Safe, for: __MODULE__ do
    def to_iodata(duration) do
      {:ok, string} = CueLightLite.Duration.format(duration.seconds)
      string
    end
  end

  @match ~r/^(?<sign>[+\-]?)(?<hour>\d+):(?<min>\d{1,2})(:(?<sec>\d{1,2}))?$/
  def parse(string) do
    case Regex.named_captures(@match, string) do
      nil ->
        :error

      %{"sign" => sign_str, "hour" => hour_str, "min" => min_str, "sec" => sec_str} ->
        with {:ok, sign} <- parse_sign(sign_str),
             {:ok, hours} <- parse_integer(hour_str),
             {:ok, minutes} <- parse_integer(min_str),
             {:ok, seconds} <- parse_integer(sec_str),
             do: validate(sign, hours, minutes, seconds)
    end
  end

  defp parse_sign(""), do: {:ok, 1}
  defp parse_sign("+"), do: {:ok, 1}
  defp parse_sign("-"), do: {:ok, -1}
  defp parse_sign(_), do: :error

  defp parse_integer(""), do: {:ok, 0}

  defp parse_integer(string) when is_binary(string) do
    case Integer.parse(string) do
      {value, _} -> {:ok, value}
      :error -> :error
    end
  end

  defp validate(sign, hours, minutes, seconds) do
    if minutes < 60 and seconds < 60 do
      {:ok, %__MODULE__{seconds: (hours * 3600 + minutes * 60 + seconds) * sign}}
    else
      :error
    end
  end

  def format(integer) when is_integer(integer) do
    sign = if integer < 0, do: "-", else: ""
    integer = abs(integer)
    hours = div(integer, 3600)
    minutes = div(integer - hours * 3600, 60)
    seconds = integer - hours * 3600 - minutes * 60

    {:ok, "#{sign}#{pad(hours)}:#{pad(minutes)}:#{pad(seconds)}"}
  end

  def format(_), do: :error

  defp pad(integer) do
    to_string(integer) |> String.pad_leading(2, ["0"])
  end
end
