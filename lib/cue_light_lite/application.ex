defmodule CueLightLite.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    topologies = Application.get_env(:libcluster, :topologies) || []

    children = [
      # Start Libcluster Supervisor
      {Cluster.Supervisor, [topologies, [name: CueLightLite.ClusterSupervisor]]},
      CueLightLite.MnesiaClusterSupervisor,
      CueLightLite.RundownScheduler.Handoff,
      CueLightLite.RundownScheduler.DynamicSupervisor,
      CueLightLite.RundownScheduler.Registry,
      CueLightLite.RundownScheduler.NodeListener,
      # Start the Ecto repository
      CueLightLite.Repo,
      # Start the Telemetry supervisor
      CueLightLiteWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: CueLightLite.PubSub},
      # Start the Endpoint (http/https)
      CueLightLiteWeb.Endpoint
      # Start a worker by calling: CueLightLite.Worker.start_link(arg)
      # {CueLightLite.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CueLightLite.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    CueLightLiteWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
