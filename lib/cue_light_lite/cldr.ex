defmodule CueLightLiteWeb.Cldr do
  @moduledoc false

  use Cldr,
    # default_locale: "en",
    # locales: ["de", "de-DE", "en", "en-US"],
    gettext: CueLightLiteWeb.Gettext,
    data_dir: "./priv/cldr",
    otp_app: :cue_light_lite,
    # precompile_number_formats: ["¤¤#,##0.##"],
    # precompile_transliterations: [{:latn, :arab}, {:thai, :latn}],
    providers: [Cldr.Number, Cldr.Calendar, Cldr.DateTime, Cldr.Language, Cldr.Territory],
    # generate_docs: true,
    force_locale_download: false

  alias CueLightLiteWeb.Cldr.Locale

  def all_locales_with_prefix(prefix) do
    {:ok, regexp} = Regex.compile("^#{prefix}.*")

    Cldr.all_locale_names()
    |> Enum.filter(fn loc -> String.match?(loc, regexp) end)
  end

  def available_locales do
    CueLightLiteWeb.Cldr.known_gettext_locale_names()
    |> Enum.map(fn name ->
      {:ok, tag} = Locale.new(name)
      {name, tag}
    end)
    |> Enum.map(fn {name, tag} -> {"#{name} (#{human_name(tag)})", name} end)
  end

  defp human_name(%Cldr.LanguageTag{} = tag) do
    "#{human_language(tag)}#{human_territory(tag)}"
  end

  defp human_language(%Cldr.LanguageTag{language: language} = _tag) do
    case CueLightLiteWeb.Cldr.Language.known_languages()
         |> Map.get(language) do
      %{standard: standard} ->
        standard

      nil ->
        case language do
          "llp" -> "Lollipop"
          _ -> language
        end
    end
  end

  defp human_territory(%Cldr.LanguageTag{} = tag) do
    case CueLightLiteWeb.Cldr.Territory.from_language_tag(tag) do
      {:ok, territory} -> "/#{territory}"
      {:error, _reason} -> ""
    end
  end
end
