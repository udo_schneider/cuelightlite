defmodule CueLightLite.Users.User do
  @moduledoc false

  use Ecto.Schema
  use Pow.Ecto.Schema
  import Ecto.Changeset
  import Pow.Ecto.Schema.Changeset, only: [new_password_changeset: 3]

  schema "users" do
    pow_user_fields()

    field :given_name, :string
    field :family_name, :string
    field :time_zone, :string
    field :locale, :string

    timestamps()
  end

  @doc false
  def changeset(user_or_changeset, attrs) do
    user_or_changeset
    |> pow_user_id_field_changeset(attrs)
    |> new_password_changeset(attrs, @pow_config)
    # |> pow_changeset(attrs)
    |> cast(attrs, [:given_name, :family_name, :time_zone, :locale])
    |> validate_required([:given_name, :family_name, :time_zone, :locale])
  end

  def time_zone(nil), do: "Etc/UTC"
  def time_zone(%__MODULE__{time_zone: nil}), do: "Etc/UTC"
  def time_zone(%__MODULE__{time_zone: time_zone}), do: time_zone

  def locale(nil), do: "en"
  def locale(%__MODULE__{locale: nil}), do: "en"
  def locale(%__MODULE__{locale: locale}), do: locale
end
