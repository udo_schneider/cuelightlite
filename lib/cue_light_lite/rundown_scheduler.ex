defmodule CueLightLite.RundownScheduler do
  @moduledoc false

  use GenServer

  require Logger

  alias CueLightLite.Planning
  alias CueLightLite.Planning.Segment
  alias CueLightLite.RundownScheduler.{DynamicSupervisor, Handoff, Registry}

  @tick_interval 1000

  def child_spec(rundown_id),
    do: %{rundown_id: rundown_id, start: {__MODULE__, :start_link, [rundown_id]}}

  def start_link(rundown_id) do
    Logger.info("Starting Rundown Scheduler for Rundown #{rundown_id}")
    # note the change here in providing a name: instead of [] as the 3rd param
    GenServer.start_link(__MODULE__, rundown_id, name: via_tuple(rundown_id))
  end

  defp via_tuple(rundown_id) do
    {:via, Horde.Registry, {Registry, rundown_id}}
  end

  defp pid(rundown_id) do
    case Horde.Registry.lookup(via_tuple(rundown_id)) do
      [{pid, _}] -> pid
      [] -> nil
    end
  end

  @impl true
  def init(rundown_id) do
    Process.send_after(self(), :tick, @tick_interval)
    Process.flag(:trap_exit, true)
    Planning.subscribe()
    rundown = Planning.get_rundown!(rundown_id)

    {current_segment, last_segment} =
      case pickup(rundown_id) do
        nil ->
          Logger.info("Rundown Scheduler - No picked up current segment - assuming new start")
          broadcast(rundown_id, :started)
          reset_rundown(rundown)

        pickup_segment_id ->
          Logger.info("Rundown Scheduler - Picked up current segment - assuming respawn")
          pickup_rundown(rundown, pickup_segment_id)
      end

    handoff(rundown.id, current_segment.id)

    {
      :ok,
      %{
        rundown: rundown,
        current_segment: current_segment,
        last_segment: last_segment
      }
      |> Map.merge(calculate_times(current_segment, last_segment))
    }
  end

  @impl true
  def terminate(_reason, %{rundown: rundown}) do
    handoff(rundown.id, nil)
    broadcast(rundown.id, :stopped)
    :ok
  end

  def subscribe, do: Phoenix.PubSub.subscribe(CueLightLite.PubSub, "scheduler")

  defp broadcast(rundown_id, event),
    do:
      Phoenix.PubSub.broadcast(
        CueLightLite.PubSub,
        "scheduler",
        {{:scheduler, rundown_id}, event}
      )

  def start(rundown_id) do
    Horde.DynamicSupervisor.start_child(DynamicSupervisor, {__MODULE__, rundown_id})
  end

  def stop(rundown_id) do
    Logger.info("Stopping Rundown Scheduler for Rundown #{rundown_id}")

    case pid(rundown_id) do
      nil -> :error
      pid -> Horde.DynamicSupervisor.terminate_child(DynamicSupervisor, pid)
    end
  end

  def toggle(rundown_id) do
    Logger.info("Toggle Rundown Scheduler for Rundown #{rundown_id}")

    if started?(rundown_id) do
      stop(rundown_id)
    else
      start(rundown_id)
    end
  end

  def started?(rundown_id) do
    case pid(rundown_id) do
      nil -> false
      _ -> true
    end
  end

  def current_segment_id(rundown_id) do
    case started?(rundown_id) do
      false -> nil
      true -> GenServer.call(via_tuple(rundown_id), :current_segment_id)
    end
  end

  def next_segment(rundown_id) do
    if started?(rundown_id), do: GenServer.cast(via_tuple(rundown_id), :next_segment)
  end

  def restart_rundown(rundown_id) do
    if started?(rundown_id), do: GenServer.cast(via_tuple(rundown_id), :restart_rundown)
  end

  @impl true
  def handle_call(:current_segment_id, _from, %{current_segment: nil} = state) do
    {:reply, nil, state}
  end

  @impl true
  def handle_call(:current_segment_id, _from, %{current_segment: current_segment} = state) do
    {:reply, current_segment.id, state}
  end

  @impl true
  def handle_cast(
        :next_segment,
        %{rundown: rundown, current_segment: current_segment, last_segment: last_segment} = state
      ) do
    now = DateTime.utc_now()

    current_segment
    |> Segment.full_changeset(%{
      actual_finish: now,
      actual_duration: DateTime.diff(now, current_segment.actual_start)
    })
    |> Planning.update_segment(rundown)

    case Planning.list_segments(rundown)
         |> list_next_segment(current_segment) do
      {_, nil} ->
        broadcast(
          rundown.id,
          %{current_segment_id: nil} |> Map.merge(calculate_times(nil, last_segment))
        )

        handoff(rundown.id, nil)
        {:noreply, %{state | current_segment: nil}}

      {_, next_segment} ->
        next_segment
        |> Segment.full_changeset(%{actual_start: now})
        |> Planning.update_segment(rundown)

        broadcast(
          rundown.id,
          %{current_segment_id: next_segment.id}
          |> Map.merge(calculate_times(next_segment, last_segment))
        )

        handoff(rundown.id, next_segment.id)
        {:noreply, %{state | current_segment: next_segment}}
    end
  end

  @impl true
  def handle_cast(:restart_rundown, %{rundown: rundown} = state) do
    {current_segment, last_segment} = reset_rundown(rundown)
    {:noreply, %{state | current_segment: current_segment, last_segment: last_segment}}
  end

  @impl true
  def handle_info(
        :tick,
        %{rundown: rundown, current_segment: current_segment, last_segment: last_segment} = state
      ) do
    broadcast(
      rundown.id,
      {:tick, calculate_times(current_segment, last_segment)}
    )

    Process.send_after(self(), :tick, @tick_interval)

    {:noreply, state}
  end

  @impl true
  def handle_info({{:event, _action}, _event}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_info(
        {{:rundown, _action}, _rundown},
        %{rundown: rundown, current_segment: current_segment, last_segment: last_segment} = state
      ) do
    current_segment =
      if current_segment do
        current_segment = Planning.get_segment_with_rundown!(current_segment.id)

        broadcast(
          current_segment.rundown.id,
          %{current_segment_id: current_segment.id}
          |> Map.merge(calculate_times(current_segment, last_segment))
        )

        current_segment
      else
        nil
      end

    last_segment = Planning.list_segments(rundown) |> List.last()

    {:noreply,
     %{
       state
       | current_segment: current_segment,
         last_segment: last_segment
     }}
  end

  @impl true
  def handle_info(
        {{:segment, _action}, _segment},
        %{rundown: rundown, current_segment: current_segment, last_segment: last_segment} = state
      ) do
    current_segment =
      if current_segment do
        current_segment = Planning.get_segment_with_rundown!(current_segment.id)

        broadcast(
          current_segment.rundown.id,
          %{current_segment_id: current_segment.id}
          |> Map.merge(calculate_times(current_segment, last_segment))
        )

        current_segment
      else
        nil
      end

    last_segment = Planning.list_segments(rundown) |> List.last()

    {:noreply,
     %{
       state
       | current_segment: current_segment,
         last_segment: last_segment
     }}
  end

  defp reset_rundown(rundown) do
    segments = Planning.list_segments(rundown)

    segments
    |> Enum.each(fn segment ->
      segment
      |> Segment.full_changeset(%{actual_start: nil, actual_duration: nil, actual_finish: nil})
      |> Planning.update_segment(rundown)
    end)

    current_segment = List.first(segments)

    current_segment
    |> Segment.full_changeset(%{actual_start: DateTime.utc_now()})
    |> Planning.update_segment(rundown)

    last_segment = List.last(segments)

    broadcast(
      rundown.id,
      %{current_segment_id: current_segment.id}
      |> Map.merge(calculate_times(current_segment, last_segment))
    )

    {current_segment, List.last(segments)}
  end

  defp pickup_rundown(rundown, current_segment_id) do
    current_segment = Planning.get_segment_with_rundown!(current_segment_id)
    last_segment = Planning.list_segments(rundown) |> List.last()

    broadcast(
      rundown.id,
      %{current_segment_id: current_segment.id}
      |> Map.merge(calculate_times(current_segment, last_segment))
    )

    {current_segment, last_segment}
  end

  defp list_next_segment(list, segment) do
    Enum.reduce(
      list,
      {false, nil},
      fn each, {fetch, value} ->
        if fetch, do: {false, each}, else: {each.id == segment.id, value}
      end
    )
  end

  defp calculate_times(current_segment, last_segment) do
    time = DateTime.utc_now()

    running_time =
      if current_segment, do: DateTime.diff(current_segment.estimated_finish, time), else: nil

    over_under_time =
      if last_segment.actual_finish != nil,
        do: last_segment.actual_finish,
        else: last_segment.estimated_finish

    over_under_diff =
      if over_under_time != nil and last_segment.planned_finish != nil,
        do: DateTime.diff(over_under_time, last_segment.planned_finish),
        else: 0

    %{
      time: time,
      running_time: running_time,
      over_under_time: over_under_time,
      over_under_diff: over_under_diff
    }
  end

  defp handoff(rundown_id, current_segment_id)
       when is_integer(rundown_id) do
    Handoff.handoff(rundown_id, current_segment_id)
  end

  defp pickup(rundown_id) when is_integer(rundown_id) do
    Handoff.pickup(rundown_id)
  end
end
