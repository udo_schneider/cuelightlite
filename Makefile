default:
	true

clean_build:
	rm -vfR _build

clean_deps:
	rm -vfR  deps

clean_assets:
	rm -vfR priv/static

clean_modules:
	rm -vfR  assets/node_modules

clean_all: clean_build clean_deps clean_assets clean_modules

lollypop:
	cd priv/gettext && rm -vfR llp
	cd priv/gettext && cp -vfR de llp
	cd priv/gettext/llp/LC_MESSAGES && for file in *.po; do sed -i 's/msgstr ".*/msgstr "🍭"/g;s/"Language: .*"/"Language: llp\\n"/g' $$file ; done

ci_cd_check:
	mix test
	mix format --check-formatted
	mix credo --strict
	mix sobelow --exit
	npm run test --prefix=./assets
	npm run lint --prefix=./assets
