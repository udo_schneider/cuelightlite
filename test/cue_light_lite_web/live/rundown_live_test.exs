defmodule CueLightLiteWeb.RundownLiveTest do
  use CueLightLiteWeb.ConnCase

  import Phoenix.LiveViewTest

  alias CueLightLite.Planning
  alias CueLightLite.Users.User

  @create_attrs %{
    start: "2010-04-17T14:00:00Z",
    finish: "2010-04-17T15:00:00Z",
    title: "some title"
  }

  #  @update_attrs %{
  #    start: "2010-04-18T14:00:00Z",
  #    finish: "2010-04-18T15:00:00Z",
  #    title: "some updated title"
  #  }
  # @invalid_attrs %{finish: nil, start: nil, title: nil}

  setup %{conn: conn} do
    user = %User{email: "test@example.com"}
    conn = Pow.Plug.assign_current_user(conn, user, otp_app: :my_app)

    {:ok, conn: conn}
  end

  defp fixture(:rundown) do
    {:ok, event} = Planning.create_event(%{title: "a title", description: "a description"})
    {:ok, rundown} = Planning.create_rundown(event, @create_attrs)
    rundown
  end

  defp create_rundown(_) do
    rundown = fixture(:rundown)
    %{rundown: rundown, event: rundown.event}
  end

  describe "Index" do
    setup [:create_rundown]

    test "lists all rundowns", %{conn: conn, event: event, rundown: rundown} do
      {:ok, _index_live, html} = live(conn, Routes.rundown_index_path(conn, :index, event))

      assert html =~ "Listing Rundowns"
      assert html =~ rundown.title
    end

    test "saves new rundown", %{conn: conn, event: event} do
      {:ok, index_live, _html} = live(conn, Routes.rundown_index_path(conn, :index, event))

      assert index_live
             |> element("a", "New Rundown")
             |> render_click() =~
               "New Rundown"

      assert_patch(index_live, Routes.rundown_index_path(conn, :new, event))

      # The testframework does not yet support datetime_selects - Please fix once upstream is fixed.
      #      assert index_live
      #             |> form("#rundown-form", rundown: @invalid_attrs)
      #             |> render_change() =~ "can&apos;t be blank"
      #
      #      {:ok, _, html} =
      #        index_live
      #        |> form("#rundown-form", rundown: @create_attrs)
      #        |> render_submit()
      #        |> follow_redirect(conn, Routes.rundown_index_path(conn, :index))
      #
      #      assert html =~ "Rundown created successfully"
      #      assert html =~ "some title"
    end

    test "updates rundown in listing", %{conn: conn, event: event, rundown: rundown} do
      {:ok, index_live, _html} = live(conn, Routes.rundown_index_path(conn, :index, event))

      assert index_live
             |> element("#rundown-#{rundown.id} a.ccl-edit")
             |> render_click() =~
               "Edit Rundown"

      assert_patch(index_live, Routes.rundown_index_path(conn, :edit, rundown))

      # The testframework does not yet support datetime_selects - Please fix once upstream is fixed.
      #      assert index_live
      #             |> form("#rundown-form", rundown: @invalid_attrs)
      #             |> render_change() =~ "can&apos;t be blank"
      #
      #      {:ok, _, html} =
      #        index_live
      #        |> form("#rundown-form", rundown: @update_attrs)
      #        |> render_submit()
      #        |> follow_redirect(conn, Routes.rundown_index_path(conn, :index))
      #
      #      assert html =~ "Rundown updated successfully"
      #      assert html =~ "some updated title"
    end

    test "deletes rundown in listing", %{conn: conn, event: event, rundown: rundown} do
      {:ok, index_live, _html} = live(conn, Routes.rundown_index_path(conn, :index, event))

      assert index_live
             |> element("#rundown-#{rundown.id} a.ccl-delete")
             |> render_click()

      refute has_element?(index_live, "#rundown-#{rundown.id}")
    end
  end

  describe "Show" do
    setup [:create_rundown]

    test "displays rundown", %{conn: conn, rundown: rundown} do
      {:ok, _show_live, html} = live(conn, Routes.rundown_show_path(conn, :show, rundown))

      assert html =~ "Show Rundown"
      assert html =~ rundown.title
    end

    test "updates rundown within modal", %{conn: conn, rundown: rundown} do
      {:ok, show_live, _html} = live(conn, Routes.rundown_show_path(conn, :show, rundown))

      assert show_live
             |> element("a", "Edit")
             |> render_click() =~
               "Edit Rundown"

      assert_patch(show_live, Routes.rundown_show_path(conn, :edit, rundown))

      # The testframework does not yet support datetime_selects - Please fix once upstream is fixed.
      #      assert show_live
      #             |> form("#rundown-form", rundown: @invalid_attrs)
      #             |> render_change() =~ "can&apos;t be blank"
      #
      #      {:ok, _, html} =
      #        show_live
      #        |> form("#rundown-form", rundown: @update_attrs)
      #        |> render_submit()
      #        |> follow_redirect(conn, Routes.rundown_show_path(conn, :show, rundown))
      #
      #      assert html =~ "Rundown updated successfully"
      #      assert html =~ "some updated title"
    end
  end
end
