defmodule CueLightLiteWeb.SegmentLiveTest do
  use CueLightLiteWeb.ConnCase

  import Phoenix.LiveViewTest

  alias CueLightLite.Planning
  alias CueLightLite.Users.User

  @create_attrs %{title: "Warmup", planned_duration: "00:05:00"}
  @update_attrs %{title: "Keynote", planned_duration: "00:10:00"}
  @invalid_attrs %{title: nil, planned_duration: nil}

  setup %{conn: conn} do
    user = %User{email: "test@example.com"}
    conn = Pow.Plug.assign_current_user(conn, user, otp_app: :my_app)

    {:ok, conn: conn}
  end

  defp fixture(:segment) do
    {:ok, event} = Planning.create_event(%{title: "a title", description: "a description"})

    {:ok, rundown} =
      Planning.create_rundown(event, %{
        start: "2010-04-17T14:00:00Z",
        finish: "2010-04-17T15:00:00Z",
        title: "some title"
      })

    {:ok, segment} = Planning.create_segment(rundown, @create_attrs)
    segment
  end

  defp create_segment(_) do
    segment = fixture(:segment)
    %{segment: segment, rundown: segment.rundown, event: segment.rundown.event}
  end

  describe "Index" do
    setup [:create_segment]

    test "lists all segments", %{conn: conn, rundown: rundown, segment: segment} do
      {:ok, _index_live, html} = live(conn, Routes.segment_index_path(conn, :index, rundown))

      assert html =~ "Listing Segments"
      assert html =~ "#{segment.position}"
    end

    test "saves new segment", %{conn: conn, rundown: rundown} do
      {:ok, index_live, _html} = live(conn, Routes.segment_index_path(conn, :index, rundown))

      assert index_live |> element("a", "New Segment") |> render_click() =~
               "New Segment"

      assert_patch(index_live, Routes.segment_index_path(conn, :new, rundown))

      assert index_live
             |> form("#segment-form", segment: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#segment-form", segment: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.segment_index_path(conn, :index, rundown))

      assert html =~ "Segment created successfully"
      assert html =~ "00:05:00"
    end

    test "updates segment in listing", %{conn: conn, rundown: rundown, segment: segment} do
      {:ok, index_live, _html} = live(conn, Routes.segment_index_path(conn, :index, rundown))

      assert index_live |> element("#segment-#{segment.id} a.ccl-edit") |> render_click() =~
               "Edit Segment"

      assert_patch(index_live, Routes.segment_index_path(conn, :edit, segment))

      assert index_live
             |> form("#segment-form", segment: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#segment-form", segment: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.segment_index_path(conn, :index, rundown))

      assert html =~ "Segment updated successfully"
      assert html =~ "00:10:00"
    end

    test "deletes segment in listing", %{conn: conn, rundown: rundown, segment: segment} do
      {:ok, index_live, _html} = live(conn, Routes.segment_index_path(conn, :index, rundown))

      assert index_live |> element("#segment-#{segment.id} a.ccl-delete") |> render_click()
      refute has_element?(index_live, "#segment-#{segment.id}")
    end
  end
end
