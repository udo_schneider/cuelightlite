defmodule CueLightLite.DurationTest do
  use ExUnit.Case

  alias CueLightLite.Duration

  test "validating hours" do
    assert Duration.cast(":") == :error
    refute Duration.cast("24:00") == :error
    refute Duration.cast("-4:00") == :error
    refute Duration.cast("100:00") == :error
    refute Duration.cast("23:00") == :error
    refute Duration.cast("0:0") == :error
  end

  test "validating minutes" do
    assert Duration.cast("00:60") == :error
    assert Duration.cast("00:60:00") == :error
    refute Duration.cast("00:59") == :error
    refute Duration.cast("00:59:00") == :error
  end

  test "validating seconds" do
    assert Duration.cast("00:00:60") == :error
    assert Duration.cast("00:00:-9") == :error
    refute Duration.cast("00:00:34") == :error
  end

  test "parsing hours and minutes" do
    assert Duration.cast("01:59") == {:ok, %Duration{seconds: 7140}}
    assert Duration.cast("20:00") == {:ok, %Duration{seconds: 72_000}}
    assert Duration.cast("00:10") == {:ok, %Duration{seconds: 600}}
    assert Duration.cast("00:00:10") == {:ok, %Duration{seconds: 10}}

    assert Duration.cast("+01:59") == {:ok, %Duration{seconds: 7140}}
    assert Duration.cast("+20:00") == {:ok, %Duration{seconds: 72_000}}
    assert Duration.cast("+00:10") == {:ok, %Duration{seconds: 600}}
    assert Duration.cast("+00:00:10") == {:ok, %Duration{seconds: 10}}

    assert Duration.cast("-01:59") == {:ok, %Duration{seconds: -7140}}
    assert Duration.cast("-20:00") == {:ok, %Duration{seconds: -72_000}}
    assert Duration.cast("-00:10") == {:ok, %Duration{seconds: -600}}
    assert Duration.cast("-00:00:10") == {:ok, %Duration{seconds: -10}}
  end

  test "loading" do
    assert Duration.load(7140) == {:ok, %Duration{seconds: 7140}}
    assert Duration.load(72_000) == {:ok, %Duration{seconds: 72_000}}
    assert Duration.load(600) == {:ok, %Duration{seconds: 600}}
    assert Duration.load(10) == {:ok, %Duration{seconds: 10}}

    assert Duration.load(-7140) == {:ok, %Duration{seconds: -7140}}
    assert Duration.load(-72_000) == {:ok, %Duration{seconds: -72_000}}
    assert Duration.load(-600) == {:ok, %Duration{seconds: -600}}
    assert Duration.load(-10) == {:ok, %Duration{seconds: -10}}
  end
end
