defmodule CueLightLite.PlanningTest do
  use CueLightLite.DataCase

  alias CueLightLite.Duration
  alias CueLightLite.Planning
  alias CueLightLite.Planning.{Event, Rundown, Segment}

  @valid_event_attrs %{description: "some description", title: "some title"}
  @update_event_attrs %{description: "some updated description", title: "some updated title"}
  @invalid_event_attrs %{description: nil, title: nil}

  @valid_rundown_attrs %{
    start: "2021-06-07T08:00:00Z",
    finish: "2021-06-07T18:00:00Z",
    title: "some title"
  }
  @update_rundown_attrs %{
    start: "2021-06-08T09:00:00Z",
    finish: "2021-06-08T19:00:00Z",
    title: "some updated title"
  }
  @invalid_rundown_attrs %{finish: nil, start: nil, title: nil}

  @valid_segment_attrs %{title: "Warmup", planned_duration: "00:05:00"}
  @update_segment_attrs %{title: "Keynote", planned_duration: "00:10:00"}
  @invalid_segment_attrs %{title: nil, planned_start: nil, planned_duration: nil}

  def event_fixture(attrs \\ %{}) do
    attrs = Enum.into(attrs, @valid_event_attrs)
    {:ok, event} = Planning.create_event(attrs)
    event
  end

  def rundown_fixture(%Event{} = event, attrs \\ %{}) do
    attrs = Enum.into(attrs, @valid_rundown_attrs)
    {:ok, rundown} = Planning.create_rundown(event, attrs)
    rundown
  end

  def segment_fixture(%Rundown{} = rundown, attrs \\ %{}) do
    attrs = Enum.into(attrs, @valid_segment_attrs)
    {:ok, segment} = Planning.create_segment(rundown, attrs)
    segment
  end

  describe "events" do
    test "list_events/0 returns all events" do
      event = event_fixture()
      assert Planning.list_events() == [event]
    end

    test "list_events/0 returns sorted events (by title, ascending)" do
      event1 = event_fixture(%{title: "Z Event"})
      event2 = event_fixture(%{title: "A Event"})
      event3 = event_fixture(%{title: "M Event"})

      titles =
        Planning.list_events()
        |> Enum.map(fn event -> event.title end)

      assert titles == [event2.title, event3.title, event1.title]
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Planning.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates a event" do
      assert {:ok, %Event{} = event} = Planning.create_event(@valid_event_attrs)
      assert event.description == "some description"
      assert event.title == "some title"
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Planning.create_event(@invalid_event_attrs)
    end

    test "update_event/2 with valid data updates the event" do
      event = event_fixture()
      assert {:ok, %Event{} = event} = Planning.update_event(event, @update_event_attrs)
      assert event.description == "some updated description"
      assert event.title == "some updated title"
    end

    test "update_event/2 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Planning.update_event(event, @invalid_event_attrs)
      assert event == Planning.get_event!(event.id)
    end

    test "delete_event/1 deletes the event" do
      event = event_fixture()
      assert {:ok, %Event{}} = Planning.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Planning.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Planning.change_event(event)
    end

    test "timestamps are in UTC" do
      event = event_fixture()
      %{time_zone: time_zone} = event.inserted_at
      assert time_zone == "Etc/UTC"

      %{time_zone: time_zone} = event.updated_at
      assert time_zone == "Etc/UTC"
    end

    test "new has no rundowns" do
      event =
        event_fixture()
        |> Repo.preload(:rundowns)

      assert event.rundowns == []
    end

    test "delete removes rundowns" do
      event = event_fixture()

      rundown1 =
        rundown_fixture(
          event,
          %{
            start: "2010-04-17T14:00:00Z",
            finish: "2010-04-17T15:00:00Z",
            title: "some title"
          }
        )

      rundown2 =
        rundown_fixture(
          event,
          %{
            start: "2010-04-18T14:00:00Z",
            finish: "2010-04-18T15:00:00Z",
            title: "Another title"
          }
        )

      {:ok, event} = Planning.update_event(event, %{rundowns: [rundown1, rundown2]})

      event =
        Planning.get_event!(event.id)
        |> Repo.preload(:rundowns)

      query = from r in Rundown, where: r.event_id == ^event.id
      rundowns = Repo.all(query)

      assert Enum.count(rundowns) == 2

      Planning.delete_event(event)

      query = from r in Rundown, where: r.event_id == ^event.id
      rundowns = Repo.all(query)

      assert Enum.empty?(rundowns)
    end
  end

  describe "rundowns" do
    test "list_rundowns/0 returns all rundowns" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      rundown = Planning.get_rundown!(rundown.id)
      assert Planning.list_rundowns(event) == [rundown]
    end

    test "list_rundowns/0 returns all rundowns sorted by start time" do
      event = event_fixture()

      rundown1 =
        rundown_fixture(event, %{start: "2010-04-17T14:00:00Z", finish: "2010-04-17T15:00:00Z"})

      rundown2 =
        rundown_fixture(event, %{start: "2010-04-16T14:00:00Z", finish: "2010-04-16T15:00:00Z"})

      ids =
        Planning.list_rundowns(event)
        |> Enum.map(fn rundown -> rundown.id end)

      assert ids == [rundown2.id, rundown1.id]
    end

    test "get_rundown!/1 returns the rundown with given id" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      assert Planning.get_rundown_with_event!(rundown.id) == rundown
    end

    test "create_rundown/1 with valid data creates a rundown" do
      event = event_fixture()
      assert {:ok, %Rundown{} = rundown} = Planning.create_rundown(event, @valid_rundown_attrs)
      assert rundown.title == "some title"
      assert rundown.start == DateTime.from_naive!(~N[2021-06-07 08:00:00], "Etc/UTC")
      assert rundown.finish == DateTime.from_naive!(~N[2021-06-07 18:00:00], "Etc/UTC")
    end

    test "create_rundown/1 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Planning.create_rundown(event, @invalid_rundown_attrs)
    end

    test "create_rundown/1 with invalid datetimes returns error changeset" do
      event = event_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Planning.create_rundown(event, %{
                 title: "New Rundown",
                 start: ~N[2021-02-03 09:00:00],
                 finish: ~N[2021-02-03 08:00:00]
               })
    end

    test "update_rundown/2 with valid data updates the rundown" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      assert {:ok, %Rundown{} = rundown} = Planning.update_rundown(rundown, @update_rundown_attrs)
      assert rundown.title == "some updated title"
      assert rundown.start == DateTime.from_naive!(~N[2021-06-08 09:00:00], "Etc/UTC")
      assert rundown.finish == DateTime.from_naive!(~N[2021-06-08 19:00:00], "Etc/UTC")
    end

    test "update_rundown/2 with invalid data returns error changeset" do
      event = event_fixture()
      rundown = rundown_fixture(event)

      assert {:error, %Ecto.Changeset{}} =
               Planning.update_rundown(rundown, @invalid_rundown_attrs)

      assert rundown == Planning.get_rundown_with_event!(rundown.id)
    end

    test "delete_rundown/1 deletes the rundown" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      assert {:ok, %Rundown{}} = Planning.delete_rundown(rundown)
      assert_raise Ecto.NoResultsError, fn -> Planning.get_rundown!(rundown.id) end
    end

    test "change_rundown/1 returns a rundown changeset" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      assert %Ecto.Changeset{} = Planning.change_rundown(rundown)
    end

    test "timestamps are in UTC" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      %{time_zone: time_zone} = rundown.inserted_at
      assert time_zone == "Etc/UTC"

      %{time_zone: time_zone} = rundown.updated_at
      assert time_zone == "Etc/UTC"
    end
  end

  describe "segments" do
    test "list_segments/0 returns all segments" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment = segment_fixture(rundown)
      segment = Planning.get_segment!(segment.id)
      assert Planning.list_segments(rundown) == [segment]
    end

    test "list_segments/0 returns sorted segments (by position, ascending)" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment1 = segment_fixture(rundown)
      segment2 = segment_fixture(rundown)
      segment3 = segment_fixture(rundown)

      positions =
        Planning.list_segments(rundown)
        |> Enum.map(fn segment -> segment.position end)

      assert positions == [segment1.position, segment2.position, segment3.position]
    end

    test "get_segment!/1 returns the segment with given id" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment = segment_fixture(rundown)
      assert Planning.get_segment_with_rundown_and_event!(segment.id) == segment
    end

    test "get_last_segment/1 returns the last segment in the rundown" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      _segment1 = segment_fixture(rundown)
      _segment2 = segment_fixture(rundown)
      segment3 = segment_fixture(rundown)
      assert Planning.get_last_segment(rundown).id == segment3.id
    end

    test "create_segment/1 with valid data creates a segment" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      assert {:ok, %Segment{} = segment} = Planning.create_segment(rundown, @valid_segment_attrs)
      assert segment.position == 1
      assert segment.planned_start == ~U[2021-06-07T08:00:00Z]
      assert segment.planned_duration == %Duration{seconds: 300}
      assert segment.planned_finish == ~U[2021-06-07T08:05:00Z]
    end

    test "create_segment/1 with invalid data returns error changeset" do
      event = event_fixture()
      rundown = rundown_fixture(event)

      assert {:error, %Ecto.Changeset{}} =
               Planning.create_segment(rundown, @invalid_segment_attrs)
    end

    test "update_segment/2 with valid data updates the segment" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment = segment_fixture(rundown)

      assert {:ok, %Segment{} = segment} =
               Planning.update_segment(
                 Planning.change_segment(segment, @update_segment_attrs),
                 rundown
               )

      assert segment.planned_duration == %Duration{seconds: 600}
    end

    test "update_segment/2 with invalid data returns error changeset" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment = segment_fixture(rundown)

      assert {:error, %Ecto.Changeset{}} =
               Planning.update_segment(
                 Planning.change_segment(segment, @invalid_segment_attrs),
                 rundown
               )

      assert segment == Planning.get_segment_with_rundown_and_event!(segment.id)
    end

    test "delete_segment/1 deletes the segment" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment = segment_fixture(rundown)
      assert {:ok, %Segment{}} = Planning.delete_segment(segment, rundown)
      assert_raise Ecto.NoResultsError, fn -> Planning.get_segment!(segment.id) end
    end

    test "change_segment/1 returns a segment changeset" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment = segment_fixture(rundown)
      assert %Ecto.Changeset{} = Planning.change_segment(segment)
    end

    test "timestamps are in UTC" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment = segment_fixture(rundown)
      %{time_zone: time_zone} = segment.inserted_at
      assert time_zone == "Etc/UTC"

      %{time_zone: time_zone} = segment.updated_at
      assert time_zone == "Etc/UTC"
    end

    test "new has no segments" do
      event = event_fixture()

      rundown =
        rundown_fixture(event)
        |> Repo.preload(:segments)

      assert rundown.segments == []
    end

    test "delete removes segments" do
      event = event_fixture()
      rundown = rundown_fixture(event)

      segment1 =
        segment_fixture(rundown, %{planned_start: ~N[2021-03-02 09:00:00], planned_duration: 300})

      segment2 =
        segment_fixture(rundown, %{planned_start: ~N[2021-03-02 09:05:00], planned_duration: 600})

      {:ok, rundown} = Planning.update_rundown(rundown, %{segments: [segment1, segment2]})

      rundown =
        Planning.get_rundown!(rundown.id)
        |> Repo.preload(:segments)

      query = from r in Segment, where: r.rundown_id == ^rundown.id
      segments = Repo.all(query)

      assert Enum.count(segments) == 2

      Planning.delete_rundown(rundown)

      query = from r in Segment, where: r.rundown_id == ^rundown.id
      segments = Repo.all(query)

      assert Enum.empty?(segments)
    end

    test "increment positions on create" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment1 = segment_fixture(rundown)
      segment2 = segment_fixture(rundown)
      segment3 = segment_fixture(rundown)

      positions = [segment1.position, segment2.position, segment3.position]

      assert positions == [1, 2, 3]
    end

    test "renumber positions on delete" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      _segment1 = segment_fixture(rundown)
      _segment2 = segment_fixture(rundown)
      segment3 = segment_fixture(rundown)
      _segment4 = segment_fixture(rundown)

      Planning.delete_segment(segment3, rundown)

      positions =
        Planning.list_segments(rundown)
        |> Enum.map(fn segment -> segment.position end)

      assert positions == [1, 2, 3]
    end

    test "renumber on segment to top" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment1 = segment_fixture(rundown)
      segment2 = segment_fixture(rundown)
      segment3 = segment_fixture(rundown)
      segment4 = segment_fixture(rundown)

      Planning.reorder_segment_to_top(rundown, segment3.id)

      ids =
        Planning.list_segments(rundown)
        |> Enum.map(fn segment -> segment.id end)

      assert ids == [segment3.id, segment1.id, segment2.id, segment4.id]
    end

    test "renumber on segment up" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment1 = segment_fixture(rundown)
      segment2 = segment_fixture(rundown)
      segment3 = segment_fixture(rundown)
      segment4 = segment_fixture(rundown)

      Planning.reorder_segment_up(rundown, segment3.id)

      ids =
        Planning.list_segments(rundown)
        |> Enum.map(fn segment -> segment.id end)

      assert ids == [segment1.id, segment3.id, segment2.id, segment4.id]
    end

    test "renumber on segment down" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment1 = segment_fixture(rundown)
      segment2 = segment_fixture(rundown)
      segment3 = segment_fixture(rundown)
      segment4 = segment_fixture(rundown)

      Planning.reorder_segment_down(rundown, segment2.id)

      ids =
        Planning.list_segments(rundown)
        |> Enum.map(fn segment -> segment.id end)

      assert ids == [segment1.id, segment3.id, segment2.id, segment4.id]
    end

    test "renumber on segment to bottom" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment1 = segment_fixture(rundown)
      segment2 = segment_fixture(rundown)
      segment3 = segment_fixture(rundown)
      segment4 = segment_fixture(rundown)

      Planning.reorder_segment_to_bottom(rundown, segment2.id)

      ids =
        Planning.list_segments(rundown)
        |> Enum.map(fn segment -> segment.id end)

      assert ids == [segment1.id, segment3.id, segment4.id, segment2.id]
    end

    test "calculate segment times" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment1 = segment_fixture(rundown, %{planned_duration: 600})
      segment2 = segment_fixture(rundown, %{planned_duration: 900})

      assert segment1.planned_start == rundown.start
      assert segment1.planned_finish == DateTime.add(rundown.start, 600, :second)

      assert segment2.planned_start == DateTime.add(rundown.start, 600, :second)
      assert segment2.planned_finish == DateTime.add(rundown.start, 600 + 900, :second)
    end

    test "retime segments after rundown change" do
      event = event_fixture()
      rundown = rundown_fixture(event)
      segment = segment_fixture(rundown)

      assert segment.planned_start == rundown.start

      {:ok, updated_rundown} =
        Planning.update_rundown(rundown, %{
          start: ~N[2021-07-07 11:00:00],
          finish: ~N[2021-07-07 19:00:00]
        })

      updated_segment = Planning.get_segment!(segment.id)

      assert updated_segment.planned_start == updated_rundown.start
    end
  end
end
